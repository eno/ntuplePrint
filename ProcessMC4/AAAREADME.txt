first compile using the "make" command.

then edit scan.py.  Set parmsfile to the file for what you want to run (right now choices are parms_QQCD.txt and parms_modelA.txt)

right under that, you can change the scanning parameters
the default is just to run one cut set.  look at the commented lines to run multiple cut sets


do 
  python scan.py -s SAMPLENAME
(where SAMPLENAME must be one of the following: QCD, ModelA, or ModelB)

then do
  source massjobs.sh


do this for both QCD and a signal model

wait for all the condor jobs to finish

then go into root and to  
  .L SrootB.C
  SrootB()

