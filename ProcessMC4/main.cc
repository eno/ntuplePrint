#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <map>

#include <iomanip>
#include <locale>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
// global variables

// for root

TTree          *fChain;   //!pointer to the analyzed TTree or TChain         
Int_t           fCurrent; //!current Tree number in a TChain  


// for codes like EMJselect
float goalintlum=40.;
int nbin;
float* xsec;
int* nfiles;
std::string* binnames;
std::string* aaname;
std::string sumhist;
bool hasPre;
bool donorm;
bool blind;
bool b16003;


void  QCDhists(int icutset, int incutset, float goalintlum,int nbin,float* xsec,int* nfiles,std::string* binnames,std::string* aaname,std::string sumhist,bool hasPre,bool donorm ,bool blind,bool b16003, float HTCUT, float PT1CUT, float PT2CUT, float PT3CUT, float PT4CUT, float ACUT, std::string outdir);

std::string decomment(std::string line) {

    std::string::size_type n = line.find("#");
    if(n!= std::string::npos)
      line.erase(n);
    return line;
}

int main(int argc, char *argv[])
{ 
  //int icutset = *(argv[1])-'0';
  //int incutset = *(argv[2])-'0';
  int icutset = (int)atoi((argv[1]));
  int incutset = (int)atoi((argv[2]));
  char* parmfile = (argv[3]); 
  double HTCUT = (double)atof((argv[4]));
  double PT1CUT = (double)atof((argv[5]));
  double PT2CUT = (double)atof((argv[6]));
  double PT3CUT = (double)atof((argv[7]));
  double PT4CUT = (double)atof((argv[8]));
  double ACUT = (double)atof((argv[9]));
  char* holder = (argv[10]);
  std::string outdir(holder);
  
  std::cout<<" cut set number is "<<icutset<<std::endl;
  std::cout<<" total number cut sets is "<<incutset<<std::endl;
  std::cout<<"HT cut is "<<HTCUT<<std::endl;
  std::cout<<"PT1CUT cut is "<<PT1CUT<<std::endl;
  std::cout<<"PT2CUT cut is "<<PT2CUT<<std::endl;
  std::cout<<"PT3CUT cut is "<<PT3CUT<<std::endl;
  std::cout<<"PT4CUT cut is "<<PT4CUT<<std::endl;
  std::cout<<"ACUT cut is "<<ACUT<<std::endl;

  std::cout<<"input file is "<<parmfile<<std::endl;
  std::cout<<"output dir is "<<holder<<std::endl;;
  std::ifstream infile;
  std::string line;
  infile.open(parmfile);
  if(infile.is_open()) {
    
    
    getline(infile,line);
    std::istringstream(decomment(line))>>b16003;
    if(b16003) std::cout<<" doing exo-16-003 mode"<<std::endl;
    else std::cout<<" doing our mode"<<std::endl;

    getline(infile,line);
    std::istringstream(decomment(line))>>blind;
    if(blind) std::cout<<" blinding signal region"<<std::endl;
    else std::cout<<" not blinding"<<std::endl;


    getline(infile,line);
    std::istringstream(decomment(line))>>donorm;
    if(donorm) std::cout<<" normalizing to "<<goalintlum<<" fb-1"<<std::endl;
    else std::cout<<" not normalizing"<<std::endl;


    getline(infile,line);
    std::istringstream(decomment(line))>>hasPre;
    if(!hasPre) std::cout<<" unable to normalize since missing hasPre"<<std::endl;

    getline(infile,line);
    std::istringstream(decomment(line))>>nbin;
    std::cout<<" assuming generated in "<<nbin<<" HT bins"<<std::endl;

    xsec = new float[nbin];
    getline(infile,line);
    std::cout<<line<<std::endl;
    std::istringstream aline(decomment(line));
    for(int i=0;i<nbin;i++) aline>>xsec[i];
    for(int i=0;i<nbin;i++) std::cout<<"  xsec["<<i<<"]="<<xsec[i]<<std::endl;

    nfiles = new int[nbin];
    getline(infile,line);
    aline.str(decomment(line).c_str());
    std::cout<<line<<std::endl;
    for(int i=0;i<nbin;i++) aline>>nfiles[i];
    for(int i=0;i<nbin;i++) std::cout<<"  nfiles["<<i<<"]="<<nfiles[i]<<std::endl;
    
    binnames = new std::string[nbin];
    getline(infile,line);
    aline.str(decomment(line).c_str());
    std::cout<<line<<std::endl;
    for(int i=0;i<nbin;i++) aline>>binnames[i];
    for(int i=0;i<nbin;i++) std::cout<<"  binnames["<<i<<"]="<<binnames[i]<<std::endl;


    aaname = new std::string[nbin];
    getline(infile,line);
    aline.str(decomment(line).c_str());
    std::cout<<line<<std::endl;
    for(int i=0;i<nbin;i++) aline>>aaname[i];
    for(int i=0;i<nbin;i++) std::cout<<" aaname["<<i<<"]="<<aaname[i]<<std::endl;

    //std::string sumhist_temp;
    getline(infile,line);
    aline.str(decomment(line).c_str());
    std::cout<<line<<std::endl;
    aline>>sumhist;
    std::cout<<"sumhist is "<<sumhist<<std::endl;
    sumhist=sumhist+"-"+std::to_string(icutset)+".root";
    std::cout<<" output histogram is "<<sumhist<<std::endl;



  }

  QCDhists(icutset,incutset,goalintlum,nbin,xsec,nfiles,binnames,aaname,sumhist,hasPre,donorm,blind,b16003, HTCUT, PT1CUT, PT2CUT, PT3CUT, PT4CUT, ACUT,outdir);


}
