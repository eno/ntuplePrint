#!/bin/bash

# requires 4 argument inputs:   
# 1: UNIQUE_ID - any unique string identifier  
# 2: CONDOR_PROCESS - condor process number  
# RUN_DIR - running directory (CMSSW_X_Y_Z/subdir)   
# mode.  should be 0 for background or 1 for signal

#
# header 
#

UNIQUE_ID=$1
CONDOR_PROCESS=$2
RUN_DIR=$3
ICUT_SET=$4
INCUT_SET=$5
PARMS_FILE=$6
HT_CUT=$7
PT1_CUT=$8
PT2_CUT=$9
PT3_CUT=${10}
PT4_CUT=${11}
ACUT=${12}
OUTDIR=${13}

echo ""
echo "CMSSW on Condor"
echo ""

START_TIME=`/bin/date`
echo "started at $START_TIME"

echo "sanity check: Outdir = $OUTDIR"

#
# setup CMSSW software environment at UMD
#
export VO_CMS_SW_DIR=/sharesoft/cmssw
. $VO_CMS_SW_DIR/cmsset_default.sh
cd $RUN_DIR
eval `scramv1 runtime -sh`

FINAL_PREFIX_NAME=`echo ${UNIQUE_ID}_${CONDOR_PROCESS}`
FINAL_LOG=`echo $FINAL_PREFIX_NAME.log`

echo "output directory will be $OUTDIR"
#
# run c
#
cd -
./main $ICUT_SET $INCUT_SET $PARMS_FILE $HT_CUT $PT1_CUT $PT2_CUT $PT3_CUT $PT4_CUT $ACUT $OUTDIR >> $FINAL_LOG 2>&1



#
# end run
#

echo ""
END_TIME=`/bin/date`
echo "finished at $END_TIME"
exit $exitcode
