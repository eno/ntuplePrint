#include <iostream>
#include <fstream>
#include <iomanip>
#include <locale>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include "vector"
using std::vector;


#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>



int EMJ16003(bool otfile, bool hasPre, const char* inputfilename,const char* outputfilename);

int EMJselect(bool otfile, bool hasPre, const char* inputfilename,const char* outputfilename,
	      float HTcut, float pt1cut, float pt2cut,float pt3cut, float pt4cut, float jetacut,float alphaMaxcut, float maxIPcut, float NemfracCut,float CemfracCut,
	      int ntrk1cut, int NemergingCut, bool blind
              );
int EMJcounter(bool otfile, bool hasPre, const char* inputfilename,const char* outputfilename,
	      float HTcut, float pt1cut, float pt2cut,float pt3cut, float pt4cut, float jetacut,float alphaMaxcut, float maxIPcut, float NemfracCut,float CemfracCut,
	      int ntrk1cut, int NemergingCut, bool blind
              );
void  HistNorm(vector<double>& norm,int nbin,float* xsec, int* nfiles, std::string* binnames, std::string outname);
TH1F* HistMan(float goalintlum,std::string thisHIST,vector<double>& histnorm, vector<double>& outnorm,int nbin,float* xsec, int* nfiles, std::string* binnames,std::string outname,bool donorm);

TH2F* HistMan2(float goalintlum,std::string thisHIST,vector<double>& histnorm, vector<double>& outnorm,int nbin,float* xsec, int* nfiles, std::string* binnames,std::string outname,bool donorm);


//std::string bbname = "./";




//void QCDhists() 
void QCDhists(int icutset, int incutset, float goalintlum,int nbin, float* xsec, int* nfiles, std::string* binnames,std::string* aaname,std::string ohname, bool hasPre,bool donorm, bool blind, bool b16003, float HTCUT, float PT1CUT, float PT2CUT, float PT3CUT, float PT4CUT, float ACUT, std::string outdir) 
{

    std::string inputfile;
    std::string outputfile;

    std::string bbname = outdir.c_str();

    //for kine scan


  
    // opt
    float DHTcut=HTCUT;
    float Dpt1cut=PT1CUT;
    float Dpt2cut=PT2CUT;
    float Dpt3cut=PT3CUT;
    float Dpt4cut=PT4CUT;
    float Dalphacut=0.04;
    float DmaxIPcut=ACUT;
    float Djetacut = 2.;
    // dont forget there is a hidden cut nalmostemergin<4!!!!!!!!!!!!!!!!!
    int Dnemcut=2;
    int Dntrk1=0;
  


  

    // make histograms for each file in each bin for the qcd sample

    std::cout<<"making histograms for each file in each bin"<<std::endl;
    vector<int> jjpass(nbin);
    for(int i=0;i<nbin;i++) jjpass[i]=0;
    for(int i=0;i<nbin;i++) {  // for each bin
      std::ifstream inputconfig(aaname[i]);
      std::cout<<"input config file is: "<<aaname[i]<<std::endl;
      int linecounter = 0;
      while(std::getline(inputconfig, inputfile))
	{
	  std::cout<<"input file is "<<inputfile<<std::endl;
	  outputfile=bbname+"histos"+binnames[i]+"-"+std::to_string(linecounter)+".root";
	  std::cout<<"output file is "<<outputfile<<std::endl;
	  int itmp;
	  if(!b16003) {
	    //itmp = EMJselect(true,hasPre,inputfile.c_str(),outputfile.c_str(),DHTcut, Dpt1cut,Dpt2cut,Dpt3cut,Dpt4cut,Djetacut,Dalphacut,DmaxIPcut,0.9,0.9,Dntrk1,Dnemcut,blind);
	    itmp = EMJcounter(true,hasPre,inputfile.c_str(),outputfile.c_str(),DHTcut, Dpt1cut,Dpt2cut,Dpt3cut,Dpt4cut,Djetacut,Dalphacut,DmaxIPcut,0.9,0.9,Dntrk1,Dnemcut,blind);
	  } else {
	    itmp = EMJ16003(true,hasPre,inputfile.c_str(),outputfile.c_str());
	  }
	  jjpass[i]=jjpass[i]+itmp;
	  linecounter += 1;
        } // end while
    } // for each bin



    // get normalization
    //  double norm[nbin];
    vector<double> norm(nbin);
    if(donorm) {
      HistNorm(norm,nbin,xsec,nfiles,binnames, bbname);  // this gives the total number of events in each bin before all selections using the eventCountPreTrigger histogram
    } else{
        for(int i=0;i<nbin;i++) norm[i]=1.;
    }
    for(int i=0;i<nbin;i++) {
        std::cout<<"total number events in bin "<<i<<" is "<<norm[i]<<std::endl;
    }
    TH1F* countclone = new TH1F("countclone","unnormalized count",20,0,20);
    for(int i=0;i<nbin;i++){
        countclone->AddBinContent(i+1,norm[i]);
    }
  
    TH1F* normhst = new TH1F("normhst","counts pretrigger by bin",nbin,0.,nbin);
    for(int i=0;i<nbin;i++){
        normhst->AddBinContent(i+1,norm[i]);
    }


    //make and  output summed and renormalized histograms
    std::cout<<"normalizing histograms"<<std::endl;
    const int nhist=2; //originally 75
    std::vector<TH1F*> vv(nhist);
    std::string histnames[nhist]={
        "count","acount"/*,"hjetcut","hjetchf","h_nemg",
        "hnjet","hpt","heta","heta2","halpha",
        "H_T","H_T2","hpt1","hpt2","hpt3",
        "hpt4","hbcut_ntrkpt1","hacut_ntrkpt1","hbcut_nef","hacut_nef",
        "hbcut_cef","hacut_cef","hbcut_alphamax","hacut_alphamax","hHTnm1",
        "hpt1nm1","hpt2nm1","hpt3nm1","hpt4nm1","halphanm1",
        "hmaxipnm1","hnHitsnm1","hntrk1nm1","hnemnm1","hipXYEJ",
        "hipXYnEJ","htvwEJ","htvw","hipXYSigEJ","hipXYSignEJ",
        "hmaxipXYEJ","hmaxipXYnEJ","hmeanipXYEJ","hmeanipXYnEJ","hnmaxipnm1",
        "hn2maxipnm1","H_T3","H_T4","hjptfrb","hjptfra1",
        "hjptfra2","hjptfrbc","hjptfra1c","hjptfra2c","hjptb",
        "hjpta","haMgj","hHTko","hpt1ko","hpt2ko",
        "hpt3ko","hpt4ko","hmass","hlogmedipXYSigEJ","hlogmedipXYSignEJ","hlogmeanipXYSigEJ","hlogmeanipXYSignEJ",
        "hmedipXYSigEJ","hmedipXYSignEJ","hmeanipXYSigEJ","hmeanipXYSignEJ","hmedipXYEJ","hmedipXYnEJ","hmeanipXYEJ","hmeanipXYnEJ"*/
    };
    vector<double> outnorm(nbin);
    for(int i=0;i<nhist;i++) {
        std::cout<<" enering Histman with i = "<<i<<": "<<histnames[i]<<std::endl;
        vv[i]=HistMan(goalintlum,histnames[i],norm,outnorm,nbin,xsec,nfiles,binnames,bbname,donorm);
    }


    //uncomment these lines concerning vv2 for regular running!
    //commented out for performing kinematic scan
    //const int nhist2=4;
    //std::vector<TH2F*> vv2(nhist2);
    //std::string histnames2[nhist2]={
    //    "aMip","haMvjpt","haMvHT","haMvnvtx"
    //};

    //vector<double> outnorm2(nbin);
    //for(int i=0;i<nhist2;i++) {
    //    std::cout<<" enering Histman2 with i = "<<i<<": "<<histnames[i]<<std::endl;
    //    vv2[i]=HistMan2(goalintlum,histnames2[i],norm,outnorm2,nbin,xsec,nfiles,binnames,bbname,donorm);
    //}

    // output total event count
    std::cout<<" initial event count before and after norm is"<<std::endl;
    double ttotal=0;
    for(int i=0;i<nbin;i++) {
        std::cout<<" bin "<<i<<" norm "<<norm[i]<<" times outnorm is "<<norm[i]*outnorm[i]<<std::endl;
        ttotal = ttotal + norm[i]*outnorm[i];
    }
    std::cout<<"total is "<<ttotal<<std::endl;;

    
    // normalize cut scan and sum bins
    std::cout<<"normalizing kinematic cut stuff"<<std::endl;
    std::cout<<"incutset "<<incutset<<std::endl;
    float totalsum=0;
    for(int i=0;i<nbin;i++) {
      totalsum+=jjpass[i]*outnorm[i];
    }
    std::cout<<" total passing is "<<totalsum<<std::endl;
    TH1F* kcutscan = new TH1F("kcutscan","n pass versus cut kin cuts",incutset,0.,incutset);
    kcutscan->AddBinContent(icutset+1,totalsum);
    




    std::cout<<"outputting histograms"<<std::endl;
    outputfile=bbname+ohname;
    std::cout<<" output file name is "<<ohname<<std::endl;
    TFile out(outputfile.c_str(),"RECREATE");
    normhst->Write();
    kcutscan->Write();
    countclone->Write();
    for(int i=0;i<nhist;i++) {
        vv[i]->Write();
    }
    //for(int i=0;i<nhist2;i++) {
    //    vv2[i]->Write();
    //}


    return;
}



TH1F* HistMan(float goalintlum,std::string thisHIST,vector<double>& norm,vector<double>& outnorm,int nbin,float* xsec, int* nfiles, std::string* binnames,std::string outname,bool donorm) {

    std::string inputfile;


    // now add up all the files for one bin
    vector<TH1F> sum(nbin);
    for(int i=0;i<nbin;i++) {  // for each bin
        for(int j=0;j<nfiles[i];j++) { //for each file for that bin
            inputfile=outname+"histos"+binnames[i]+"-"+std::to_string(j)+".root";
            TFile* in = new TFile(inputfile.c_str());
            if (!in->GetListOfKeys()->Contains(thisHIST.c_str())) return (new TH1F(thisHIST.c_str(),"dummy empty hist",10,0.,10.));

            if(j==0) {
                std::cout<<" adding up histos within a bin"<<std::endl;
                sum[i] = *(static_cast<TH1F*>(in->Get(thisHIST.c_str())->Clone()));
            } else {
                TH1F* tmp = static_cast<TH1F*>(in->Get(thisHIST.c_str())->Clone());
                sum[i].Add(tmp);
            }
            in->Close();
        }
    }

    if(donorm) {
        // reweight to int lum
        std::cout<<" reweighting to inst lum of "<<goalintlum<<" for each bin"<<std::endl;
        for(int i=0;i<nbin;i++) {
            // get total number of events before filter
            float ntotal = norm[i];
            std::cout<<" for bin "<<i<<" number of pretrigger events is "<<ntotal<<std::endl;
            float fileLum= ntotal/xsec[i];
            std::cout<<" equ lum for bin is "<<fileLum<<" fb-1"<<std::endl;
            outnorm[i] = goalintlum/fileLum;
            std::cout<<" scaling by a factor of "<<outnorm[i]<<std::endl;
            sum[i].Scale(outnorm[i]);
        }
    }

    //add the bins
    std::cout<<" adding bins"<<std::endl;
    TH1F* SUM=static_cast<TH1F*>((sum[0]).Clone());
    for(int i=1;i<nbin;i++) {
        SUM->Add(&sum[i]);
    }


    return SUM;
}

TH2F* HistMan2(float goalintlum,std::string thisHIST,vector<double>& norm,vector<double>& outnorm,int nbin,float* xsec, int* nfiles, std::string* binnames,std::string outname,bool donorm) {

    std::string inputfile;


    // now add up all the files for one bin
    std::cout<<" adding up histos within a bin"<<std::endl;
    vector<TH2F> sum(nbin);
    for(int i=0;i<nbin;i++) {  // for each bin
        for(int j=0;j<nfiles[i];j++) { //for each file for that bin
            inputfile=outname+"histos"+binnames[i]+"-"+std::to_string(j)+".root";
            TFile* in = new TFile(inputfile.c_str());
            if(j==0) {
                sum[i] = *(static_cast<TH2F*>(in->Get(thisHIST.c_str())->Clone()));
            } else {
                TH2F* tmp = static_cast<TH2F*>(in->Get(thisHIST.c_str())->Clone());
                sum[i].Add(tmp);
            }
            in->Close();
        }
    }

    if(donorm) {
        // reweight to int lum
        std::cout<<" reweighting to inst lum of "<<goalintlum<<" for each bin"<<std::endl;
        for(int i=0;i<nbin;i++) {
            // get total number of events before filter
            float ntotal = norm[i];
            std::cout<<" for bin "<<i<<" number of pretrigger events is "<<ntotal<<std::endl;
            float fileLum= ntotal/xsec[i];
            std::cout<<" equ lum for bin is "<<fileLum<<" fb-1"<<std::endl;
            outnorm[i] = goalintlum/fileLum;
            std::cout<<" scaling by a factor of "<<outnorm[i]<<std::endl;
            sum[i].Scale(outnorm[i]);
        }
    }


    //add the bins
    std::cout<<" adding bins"<<std::endl;
    TH2F* SUM=static_cast<TH2F*>((sum[0]).Clone());
    for(int i=1;i<nbin;i++) {
        SUM->Add(&sum[i]);
    }


    return SUM;
}

void  HistNorm(vector<double>& norm,int nbin,float* xsec, int* nfiles, std::string* binnames, std::string outname) {

    std::cout<<"entering HistNorm"<<std::endl; 

    std::string inputfile;
    TFile * in;

    // now add up all the files for one bin
    vector<TH1F> sum(nbin);
    for(int i=0;i<nbin;i++) {  // for each bin
        for(int j=0;j<nfiles[i];j++) { //for each file for that bin
            inputfile=outname+"histos"+binnames[i]+"-"+std::to_string(j)+".root";
            std::cout<<i<<" "<<j<<" "<<inputfile<<std::endl;
            in = new TFile(inputfile.c_str());
            if(j==0) {
                sum[i] = *(static_cast<TH1F*>(in->Get("eventCountPreTrigger")->Clone()));
            } else {
                TH1F* tmp = static_cast<TH1F*>(in->Get("eventCountPreTrigger")->Clone());
                sum[i].Add(tmp);
            }
            in->Close();
        }
    }

    // reweight to int lum

    for(int i=0;i<nbin;i++) {
        // get total number of events before filter
        norm[i] = sum[i].GetBinContent(2);
        std::cout<<"norm "<<i<<" "<<norm[i]<<std::endl;
    }


    return;
}
