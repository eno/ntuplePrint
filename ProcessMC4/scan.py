#! /usr/bin/env/python
# for some reason now you have to run it twice.  
# it seems the .txt1 files are not quite there when the cat commands comes along

import subprocess
import os
import shutil
import sys
import optparse
#from optparse import OptionParser, OptionGroup

parser = optparse.OptionParser(usage="usage: python scan.py -s [QCD, ModelA, ModelB] ",description="This is a script to set up and run a scan over the kinematic cuts in the emerging jets analysis.")
parser.add_option ('-s','--sample', type='string', dest="sample", help="Sample to process (e.g., QCD)")

options, args = parser.parse_args()
if len(args) < 0:
  print "Too few arguments"
  parser.print_usage()
  exit(1)

#hostarea = "/data/users/eno/emergingJets5/CMSSW_7_6_3/src/EmergingJetAnalysis/histsQCD/"
#exearea = "/data/users/eno/emergingJets5/CMSSW_7_6_3/src/EmergingJetAnalysis/"
mainarea = os.getcwd()
hostarea = os.getcwd()
hostarea+="/"


#Set the output directory name and parameter file based on the sample you are running over
dirname=""
parmsfile=""
if options.sample=="QCD":
  dirname="kinscan_QCD"
  parmsfile = "parms_QQCD.txt"
elif options.sample=="ModelA":
  dirname="kinscan_modA"
  parmsfile = "parms_modelA.txt"
elif options.sample=="ModelB":
  dirname="kinscan_modB"
  parmsfile = "parms_modelB.txt"
else:
  print("Input sample not recognized!")
  parser.print_usage()
  exit(1)

#Create output directory; ask what to do if it exists already
print('Checking if directory {} exists...'.format(dirname))
if os.path.exists(hostarea+dirname):
  userreply = raw_input("Output directory already exists! Are you sure you want to overwrite all the contents? (y/n): ")
  if (userreply == "y"):
    print("Answer = yes. Old output directory will be deleted and a new empty one will be created.")
    shutil.rmtree(hostarea+dirname)
    os.makedirs(hostarea+dirname)
  elif (userreply == "n"):
    print("Answer = no. Exiting scan.py.")
    exit() 
  else:
    print("Invalid input. Exiting scan.py anyway.")
    exit()
else:
  os.makedirs(hostarea+dirname)

hostarea=hostarea+dirname+"/"

#Configure the scan

nkincut=6; #was 5
#nstep = [1,1,1,1,1]
#cutmin = [1000., 400., 200., 200., 100.]
#ss = [500,10,10,10,20]
#jcut = [0,0,0,0,0,0]


nstep = [1,1,1,1,1,2] #was [1,1,1,1,1]
cutmin = [1000., 400., 200., 200., 100., 0.] #was without last element
ss = [250,10,10,10,10,0.01] #was without last element
jcut = [0,0,0,0,0,0,0] #was [0,0,0,0,0,0]


def decode(icode,ncut, ss,jcut):
  kcut = [0,0,0,0,0,0,0] #was [0,0,0,0,0,0]
  print "decoding code= "+str(icode)
  #print "number of variables cut on is "+str(ncut)
  itmp=icode
  for j in range(0,ncut):   # this will do 0,1,...,ncut-1
    izz=1;
    for i in range(j,ncut):  # here we calculate the products of nsteps.  for the msb all, for the lsb just itself
      izz=izz*nstep[i]
    #print "for j "+str(j)+" izz is "+str(izz)
    itmp2=itmp%izz   # starting with the mwb, take mod of code with its product
    itmp=itmp2   # take the result of the mod and pass it so can do the next mod on the result
    kcut[j]=itmp2   # store each mod
    #print "kcut["+str(j)+"]="+str(kcut[j])
  
  jcut[ncut-1]=kcut[ncut-1];  # this is the lsb of the code, the last mod
  #print "jcut["+str(ncut-1)+"]="+str(jcut[ncut-1])

  #find the bits from the inner most-1 to the outer most                            
  # (the inner most, ncut-1, already done)                                           

  for k in range(0,ncut-1):  # jcut[ncut-1] set earlier
    i=ncut-k-2  # from the second to lsb to the msb
    #print "calculating bit "+str(i)
    #print "kcut["+str(i)+"]="+str(kcut[i])+" jcut[ncut-1]="+str(jcut[ncut-1])
    ia=kcut[i]-jcut[ncut-1]
    #print "ia="+str(ia)
    for j in range(i+1,ncut-2):
      #print "  j is ="+str(j)+" jcut["+str(j+1)+"]="+str(jcut[j+1])+" nstep["+str(j+1)+"]="+str(nstep[j+1])
      ia=ia-jcut[j]*nstep[j]
      #print "ia is "+str(ia)
    for j in range(i+1,ncut-1):
      #print "lala ia is "+str(ia)
      #print "2nd j is "+str(j)+" nstep["+str(j)+"]="+str(nstep[j])
      ia=ia/nstep[j]
      #print "ia is now "+str(ia)
    jcut[i]=ia
    #print "jcut["+str(i)+"]="+str(jcut[i])


  #print "result is "
  #for i in range(0,ncut):
  #  print "jcut["+str(i)+"]="+str(jcut[i])

  return;


#iicut = total number of scan points in cut scan
iicut =1
for i in nstep:
  iicut=iicut*i

#write the .jdl file and the bash script to submit the condor job
f = open("massjobs.sh",'w')


for i in range(0,iicut):
  print "about to call decode for "+str(i)
  decode(i,nkincut,nstep,jcut)
  for j in range(0,nkincut):
    print "jcut["+str(j)+"]="+str(jcut[j])
  HTcut=cutmin[0]+ss[0]*jcut[0]
  pT1cut=cutmin[1]+ss[1]*jcut[1]
  pT2cut=cutmin[2]+ss[2]*jcut[2]
  pT3cut=cutmin[3]+ss[3]*jcut[3]
  pT4cut=cutmin[4]+ss[4]*jcut[4]
  acut=cutmin[5]+ss[5]*jcut[5]

  name = "kinescan-"+parmsfile+"-"+str(i)
  
  jdlfile = open(hostarea+"condor-jobs-"+name+".jdl","w")
  jdlfile.write("universe = vanilla"+'\n')
  jdlfile.write("Executable = "+mainarea+"/condor-executable.sh"+'\n')
  jdlfile.write("should_transfer_files = NO"+'\n')
  jdlfile.write("Requirements = TARGET.FileSystemDomain == \"privnet\""+'\n')
  jdlfile.write("Output = "+hostarea+name+"_sce_$(cluster)_$(process).stdout"+'\n')
  jdlfile.write("Error = "+hostarea+name+"_sce_$(cluster)_$(process).stderr"+'\n')
  jdlfile.write("Log = "+hostarea+name+"_sce_$(cluster)_$(process).condor"+'\n')
  jdlfile.write("Arguments = "+name+" $(process) "+hostarea+" "+str(i)+" "+str(iicut)+" "+parmsfile+" "+str(HTcut)+" "+str(pT1cut)+" "+str(pT2cut)+" "+str(pT3cut)+" "+str(pT4cut)+" "+str(acut)+" "+hostarea+" "+'\n')
  jdlfile.write("Queue 1"+'\n')
  jdlfile.close()

  f.write("condor_submit "+dirname+"/condor-jobs-"+name+'.jdl'+'\n')

f.close()



