#include <iostream>
#include <iomanip>
#include <locale>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include "vector"
#include "vector"
using std::vector;

#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

TTree          *fChain;   //!pointer to the analyzed TTree or TChain               
Int_t           fCurrent; //!current Tree number in a TChain                       

// Fixed size dimensions of array or collections stored in the TTree if any.       





void dumpH(const char* inputfilename, const char* hname) {
  // root file with histogram, histogram name



  TFile *f = new TFile(inputfilename);

  // get histogram of events before trigger
  TH1F* ahist = static_cast<TH1F*>(f->Get(hname)->Clone());

  int nbin = ahist->GetNbinsX();
  std::cout<<" this histogram has "<<nbin<<" bins"<<std::endl;
  for(int i=0;i<=nbin;i++) {
    float aa = ahist->GetBinContent(i);
    float bb = ahist->GetBinError(i);
    std::cout<<"bin "<<i<<" contains "<<aa<<"+-"<<bb<<std::endl;
  }


  f->Close();



}
